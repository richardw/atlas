from django.test import TestCase
from django.urls import resolve
from django.shortcuts import render_to_response
from django.test import RequestFactory

class MainPageTests(TestCase):
  
  ###############
  #### Setup ####
  ###############

  @classmethod
  def setUpClass(cls):
    super(MainPageTests, cls).setUpClass()
    request_factory = RequestFactory()
    cls.request = request_factory.get('/')
    cls.request.session = {}