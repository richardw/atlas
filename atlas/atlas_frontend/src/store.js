import Vue from "vue";
import Vuex from "vuex";
import doAsync from "./store/async-get";
import doAsyncGetOrSet from "./store/async-set";
import * as types from "./store/mutation-types";

import axios from "axios";
import { API_URL } from "./services/config";
axios.defaults.baseURL = API_URL;

Vue.use(Vuex);

const state = {
  currentTimeStep: {},
  indicatorgroups: {},
  indicatorsmeta: {},
  indicators: {},
  indicatoraggregates: {},
  indicator: {},
  legend: {},
  region_sets: {},
  region_set: {},
  regions: {},
  region: {},
  plot: {}
};

const mutations = {
  setCurrentTimeStep(state, t) {
    state.currentTimeStep = t;
  },

  [types.GET_INDICATORGROUPS_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_INDICATORGROUPS_ASYNC.loadingKey, true);
  },
  [types.GET_INDICATORGROUPS_ASYNC.FAILURE](state, error) {
    state[types.GET_INDICATORGROUPS_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_INDICATORGROUPS_ASYNC.stateKey], error);
  },
  [types.GET_INDICATORGROUPS_ASYNC.SUCCESS](state, indicatorgroups) {
    state[types.GET_INDICATORGROUPS_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_INDICATORGROUPS_ASYNC.stateKey], indicatorgroups);
    state.indicatorgroups = indicatorgroups;
  },

  [types.GET_INDICATORAGGREGATES_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_INDICATORAGGREGATES_ASYNC.loadingKey, true);
  },
  [types.GET_INDICATORAGGREGATES_ASYNC.FAILURE](state, error) {
    state[types.GET_INDICATORAGGREGATES_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_INDICATORAGGREGATES_ASYNC.stateKey], error);
  },
  [types.GET_INDICATORAGGREGATES_ASYNC.SUCCESS](state, indicatoraggregates) {
    state[types.GET_INDICATORAGGREGATES_ASYNC.loadingKey] = false;
    Vue.set(
      state,
      [types.GET_INDICATORAGGREGATES_ASYNC.stateKey],
      indicatoraggregates
    );
    state.indicatoraggregates = indicatoraggregates;
  },

  [types.GET_INDICATORS_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_INDICATORS_ASYNC.loadingKey, true);
  },
  [types.GET_INDICATORS_ASYNC.FAILURE](state, error) {
    state[types.GET_INDICATORS_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_INDICATORS_ASYNC.stateKey], error);
  },
  [types.GET_INDICATORS_ASYNC.SUCCESS](state, indicators) {
    state[types.GET_INDICATORS_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_INDICATORS_ASYNC.stateKey], indicators);
    state.indicators = indicators;
  },

  [types.GET_INDICATORSMETA_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_INDICATORSMETA_ASYNC.loadingKey, true);
  },
  [types.GET_INDICATORSMETA_ASYNC.FAILURE](state, error) {
    state[types.GET_INDICATORSMETA_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_INDICATORSMETA_ASYNC.stateKey], error);
  },
  [types.GET_INDICATORSMETA_ASYNC.SUCCESS](state, indicatorsmeta) {
    state[types.GET_INDICATORSMETA_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_INDICATORSMETA_ASYNC.stateKey], indicatorsmeta);
    state.indicatorsmeta = indicatorsmeta;
  },

  [types.GET_INDICATOR_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_INDICATOR_ASYNC.loadingKey, true);
  },
  [types.GET_INDICATOR_ASYNC.FAILURE](state, error) {
    state[types.GET_INDICATOR_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_INDICATOR_ASYNC.stateKey], error);
  },
  [types.GET_INDICATOR_ASYNC.SUCCESS](state, indicator) {
    state[types.GET_INDICATOR_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_INDICATOR_ASYNC.stateKey], indicator);
    state.indicator = indicator;
  },

  [types.GET_LEGEND_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_LEGEND_ASYNC.loadingKey, true);
  },
  [types.GET_LEGEND_ASYNC.FAILURE](state, error) {
    state[types.GET_LEGEND_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_LEGEND_ASYNC.stateKey], error);
  },
  [types.GET_LEGEND_ASYNC.SUCCESS](state, legend) {
    state[types.GET_LEGEND_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_LEGEND_ASYNC.stateKey], legend);
    state.legend = legend;
  },

  [types.GET_REGION_SETS_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_REGION_SETS_ASYNC.loadingKey, true);
  },
  [types.GET_REGION_SETS_ASYNC.FAILURE](state, error) {
    state[types.GET_REGION_SETS_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_REGION_SETS_ASYNC.stateKey], error);
  },
  [types.GET_REGION_SETS_ASYNC.SUCCESS](state, region_sets) {
    state[types.GET_REGION_SETS_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_REGION_SETS_ASYNC.stateKey], region_sets);
    state.region_sets = region_sets;
  },

  [types.GET_REGION_SET_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_REGION_SET_ASYNC.loadingKey, true);
  },
  [types.GET_REGION_SET_ASYNC.FAILURE](state, error) {
    state[types.GET_REGION_SET_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_REGION_SET_ASYNC.stateKey], error);
  },
  [types.GET_REGION_SET_ASYNC.SUCCESS](state, region_set) {
    state[types.GET_REGION_SET_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_REGION_SET_ASYNC.stateKey], region_set);
    state.region_set = region_set;
  },

  [types.GET_REGIONS_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_REGIONS_ASYNC.loadingKey, true);
  },
  [types.GET_REGIONS_ASYNC.FAILURE](state, error) {
    state[types.GET_REGIONS_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_REGIONS_ASYNC.stateKey], error);
  },
  [types.GET_REGIONS_ASYNC.SUCCESS](state, regions) {
    state[types.GET_REGIONS_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_REGIONS_ASYNC.stateKey], regions);
    state.regions = regions;
  },

  [types.GET_REGION_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_REGION_ASYNC.loadingKey, true);
  },
  [types.GET_REGION_ASYNC.FAILURE](state, error) {
    state[types.GET_REGION_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_REGION_ASYNC.stateKey], error);
  },
  [types.GET_REGION_ASYNC.SUCCESS](state, region) {
    state[types.GET_REGION_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_REGION_ASYNC.stateKey], region);
    state.region = region;
  },

  [types.GET_OR_SET_PLOT_ASYNC.PENDING](state) {
    Vue.set(state, types.GET_OR_SET_PLOT_ASYNC.loadingKey, true);
  },
  [types.GET_OR_SET_PLOT_ASYNC.FAILURE](state, error) {
    state[types.GET_OR_SET_PLOT_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_OR_SET_PLOT_ASYNC.stateKey], error);
  },
  [types.GET_OR_SET_PLOT_ASYNC.SUCCESS](state, plot) {
    state[types.GET_OR_SET_PLOT_ASYNC.loadingKey] = false;
    Vue.set(state, [types.GET_OR_SET_PLOT_ASYNC.stateKey], plot);
    state.plot = plot;
  }
};

const actions = {
  setCurrentTimeStep(store, t) {
    store.commit("setCurrentTimeStep", t);
  },

  getIndicatorGroups(store) {
    return new Promise((resolve, reject) => {
      doAsync(store, resolve, reject, {
        url: "indicatorgroups/",
        mutationTypes: types.GET_INDICATORGROUPS_ASYNC
      });
    });
  },
  getIndicatorAggregates(store, slug) {
    // console.log(
    //   `looking for indicatoraggregates/?indicator_group__key=${slug}`
    // );
    return new Promise((resolve, reject) => {
      doAsync(store, resolve, reject, {
        url: `indicatoraggregates/?indicator_group__key=${slug}`,
        mutationTypes: types.GET_INDICATORAGGREGATES_ASYNC
      });
    });
  },
  getIndicators(store, slug) {
    // console.log(`looking for indicators/?indicator_aggregate__key=${slug}`)
    return new Promise((resolve, reject) => {
      doAsync(store, resolve, reject, {
        url: `indicators/?indicator_aggregate__key=${slug}`, // indicator_group__key=${slug}
        mutationTypes: types.GET_INDICATORS_ASYNC
      });
    });
  },
  getIndicatorsmeta(store) {
    // get metadata associated with all indicators
    return new Promise((resolve, reject) => {
      doAsync(store, resolve, reject, {
        url:
          "https://land-climate-atlas.ethz.ch/geoserver/netcdf/wms?&request=GetCapabilities&service=WMS&version=1.3.0",
        mutationTypes: types.GET_INDICATORSMETA_ASYNC
      });
    });
  },
  getIndicator(store, slug) {
    return new Promise((resolve, reject) => {
      doAsync(store, resolve, reject, {
        url: `indicators/${slug}/`,
        mutationTypes: types.GET_INDICATOR_ASYNC
      });
    });
  },
  getLegend(store, slug) {
    return new Promise((resolve, reject) => {
      doAsync(store, resolve, reject, {
        url: `${slug}`,
        mutationTypes: types.GET_LEGEND_ASYNC
      });
    });
  },
  getRegionSets(store) {
    return new Promise((resolve, reject) => {
      doAsync(store, resolve, reject, {
        url: "region_sets/",
        mutationTypes: types.GET_REGION_SETS_ASYNC
      });
    });
  },
  getRegionSet(store, slug) {
    return new Promise((resolve, reject) => {
      doAsync(store, resolve, reject, {
        url: `region_sets/${slug}/`,
        mutationTypes: types.GET_REGION_SET_ASYNC
      });
    });
  },
  getRegions(store, slug) {
    return new Promise((resolve, reject) => {
      doAsync(store, resolve, reject, {
        url: `regions/?region_set=${slug}`,
        mutationTypes: types.GET_REGIONS_ASYNC
      });
    });
  },
  getRegion(store, slug) {
    return new Promise((resolve, reject) => {
      doAsync(store, resolve, reject, {
        url: `regions/${slug}/`,
        mutationTypes: types.GET_REGION_ASYNC
      });
    });
  },
  getOrSetPlot(store, data) {
    doAsyncGetOrSet(store, {
      url: "plots/",
      mutationTypes: types.GET_OR_SET_PLOT_ASYNC,
      data: data
    });
  }
};

export default new Vuex.Store({
  state,
  mutations,
  actions
});
