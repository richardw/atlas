import "@babel/polyfill";
import "@fortawesome/fontawesome-free/css/all.css";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import "@mdi/font/css/materialdesignicons.css";
import "roboto-fontface/css/roboto/roboto-fontface.css";

import Vue from "vue";
import "./plugins/vuetify";
import Meta from "vue-meta";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";

import * as L from "leaflet"; // Import Leaflet into L
import "leaflet-timedimension/dist/leaflet.timedimension.min.js"; // Import the plugin libraries so they will modify L
import "leaflet-timedimension/node_modules/iso8601-js-period/iso8601.min.js";
import "vuetify/dist/vuetify.css";
import "leaflet/dist/leaflet.css";
import "leaflet-timedimension/dist/leaflet.timedimension.control.min.css";
import "proj4leaflet";

Vue.prototype.$L = L;

Vue.config.productionTip = false;

Vue.use(Meta, {
  keyName: "metaInfo", // the component option name that vue-meta looks for meta info on.
  attribute: "data-vue-meta", // the attribute name vue-meta adds to the tags it observes
  ssrAttribute: "data-vue-meta-server-rendered", // the attribute name that lets vue-meta know that meta info has already been server-rendered
  tagIDKeyName: "vmid" // the property name that vue-meta uses to determine whether to overwrite or append a tag
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
