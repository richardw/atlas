// Libraries
import Vue from "vue";
import {
  Vuetify,
  VApp,
  VBtn,
  VCard,
  VCarousel,
  VDatePicker,
  VDialog,
  VExpansionPanel,
  VFooter,
  VGrid,
  VIcon,
  VImg,
  VList,
  VMenu,
  VNavigationDrawer,
  VProgressCircular,
  VProgressLinear,
  VResponsive,
  VSelect,
  VSheet,
  VTabs,
  VTextField,
  VToolbar,
  VTooltip,
  transitions
} from "vuetify";

// Helpers
import colors from "vuetify/es5/util/colors";
import "vuetify/src/stylus/app.styl";

Vue.use(Vuetify, {
  components: {
    VApp,
    VBtn,
    VCard,
    VCarousel,
    VDatePicker,
    VDialog,
    VExpansionPanel,
    VFooter,
    VGrid,
    VIcon,
    VImg,
    VList,
    VMenu,
    VNavigationDrawer,
    VProgressCircular,
    VProgressLinear,
    VResponsive,
    VSelect,
    VSheet,
    VTabs,
    VTextField,
    VToolbar,
    VTooltip,
    transitions
  },
  iconfont: "fa",
  theme: {
    primary: colors.red.darken2, // #E53935
    secondary: colors.red.lighten4, // #FFCDD2
    accent: colors.indigo.base // #3F51B5
  }
});
