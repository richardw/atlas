import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Map from "./views/Map.vue";
import MapControl from "./views/MapControl.vue";
import Plot from "./views/Plot.vue";
import PlotControl from "./views/PlotControl.vue";
import ExternalControl from "./views/ExternalControl.vue";
import NotFound from "./views/NotFound.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: Home
      }
    },
    {
      path: "/:indicatorGroupKey/:indicatorKey/:plotTypeKey",
      name: "map",
      components: {
        default: Map,
        mapcontrol: MapControl
      }
    },
    {
      path: "/:indicatorGroupKey/:indicatorKey/:plotTypeKey",
      name: "external",
      components: {
        default: Map,
        mapcontrol: MapControl,
        plotcontrol: ExternalControl
      }
    },
    {
      path:
        "/:indicatorGroupKey/:indicatorKey/:plotTypeKey/:statisticKey/:regionSetKey/:regionKey",
      name: "plot",
      components: {
        default: Map,
        plot: Plot,
        mapcontrol: MapControl,
        plotcontrol: PlotControl
      },
      props: route => ({ query: route.query.t }) // used for mapping current time step to the url
    },
    {
      path: "/404",
      name: "notFound",
      components: {
        default: NotFound
      }
    }
  ]
});
