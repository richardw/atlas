import axios from "axios";
import { API_URL } from "../services/config";

axios.defaults.baseURL = API_URL;

const doAsync = (store, resolve, reject, { url, mutationTypes }) => {
  store.commit(mutationTypes.PENDING);
  setTimeout(() => {
    axios(url, {})
      .then(response => {
        store.commit(mutationTypes.SUCCESS, response.data);
        resolve(response);
      })
      .catch(error => {
        if (error.response) {
          if (
            error.response.statusText == "Not Found" &&
            error.request.responseURL.includes("plots")
          ) {
            store.commit(mutationTypes.SUCCESS, error.response.statusText);
            resolve(error.response.statusText);
          } else {
            store.commit(mutationTypes.FAILURE, error.response.statusText);
            reject(error);
          }
        } else {
          store.commit(mutationTypes.FAILURE, error.message);
          reject(error);
        }
      });
  }, 500);
};

export default doAsync;
