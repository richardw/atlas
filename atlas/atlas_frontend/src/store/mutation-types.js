import _ from "lodash";

const createAsyncMutation = type => ({
  SUCCESS: `${type}_SUCCESS`,
  FAILURE: `${type}_FAILURE`,
  PENDING: `${type}_PENDING`,
  loadingKey: _.camelCase(`${type}_PENDING`),
  stateKey: _.camelCase(`${type}_DATA`)
});

export const GET_INDICATORGROUPS_ASYNC = createAsyncMutation(
  "GET_INDICATORGROUPS"
);

export const GET_INDICATORAGGREGATES_ASYNC = createAsyncMutation(
  "GET_INDICATORAGGREGATES"
);

export const GET_INDICATORSMETA_ASYNC = createAsyncMutation(
  "GET_INDICATORSMETA"
);

export const GET_INDICATORS_ASYNC = createAsyncMutation("GET_INDICATORS");
export const GET_INDICATOR_ASYNC = createAsyncMutation("GET_INDICATOR");

export const GET_LEGEND_ASYNC = createAsyncMutation("GET_LEGEND");

export const GET_REGION_SETS_ASYNC = createAsyncMutation("GET_REGION_SETS");
export const GET_REGION_SET_ASYNC = createAsyncMutation("GET_REGION_SET");

export const GET_REGIONS_ASYNC = createAsyncMutation("GET_REGIONS");
export const GET_REGION_ASYNC = createAsyncMutation("GET_REGION");

export const GET_OR_SET_PLOT_ASYNC = createAsyncMutation("GET_OR_SET_PLOT");
