import axios from "axios";
import { API_URL } from "../services/config";

axios.defaults.baseURL = API_URL;
axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.xsrfHeaderName = "X-CSRFToken";
export const HTTP = axios.create({
  baseURL: API_URL
});

const doAsyncGetOrSet = (store, { url, mutationTypes, data }) => {
  store.commit(mutationTypes.PENDING);
  // console.log("axios doAsyncGetOrSet request for " + url + data.key + "/");
  setTimeout(() => {
    axios(url + data.key + "/", {})
      .then(response => {
        store.commit(mutationTypes.SUCCESS, response.data);
      })
      .catch(error => {
        if (error.message == "Request failed with status code 404") {
          // console.log(
          //   "sending POST: " +
          //     url +
          //     ", " +
          //     data.indicator +
          //     ", " +
          //     data.region +
          //     ", " +
          //     data.statistic +
          //     ", " +
          //     data.plot_type
          // );
          HTTP.post(url, {
            indicator: data.indicator,
            region: data.region,
            plot_type: data.plot_type,
            statistic: data.statistic
          })
            .then(response => {
              store.commit(mutationTypes.SUCCESS, response.data);
            })
            .catch(error => {
              // console.log("ERROR (should not happen): " + error.message);
              if (typeof error.response === "undefined") {
                store.commit(mutationTypes.FAILURE, error.message);
              } else {
                store.commit(mutationTypes.FAILURE, error.response.statusText);
              }
            });
        } else {
          if (typeof error.response === "undefined") {
            store.commit(mutationTypes.FAILURE, error.message);
          } else {
            store.commit(mutationTypes.FAILURE, error.response.statusText);
          }
        }
      });
  }, 500);
};

export default doAsyncGetOrSet;
