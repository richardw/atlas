"""
Import geojson into the Django database to populate the available region sets and regions (only once)
"""

import os
import geojson
#import datetime
#import subprocess
#import re
#from slugify import slugify
from main.models import RegionSet, Region
from django.contrib.gis.geos import Polygon as GeosPolygon
import geopandas
from shapely.ops import cascaded_union, unary_union
from shapely.geometry import asShape, MultiPolygon

di = os.path.join("geoserver_data/region_masks/")
fl = [os.path.join(di, f) for f in os.listdir(di) if os.path.isfile(os.path.join(di, f))]
fi = fl[5]

longNames = {
	'SREX' : 'Special Report on Extremes Regions',
    'PRUDENCE' : 'PRUDENCE Regions',
    'NUTS' : 'Nomenclature of Territorial Units for Statistics',
    'Continents' : 'Global Continents',
    'Countries' : 'Countries',
}
primaryRegions = {
	'SREX' : 'ceu-land',
    'PRUDENCE' : 'al-land',
    'NUTS' : 'ch0-land',
    'Continents' : 'europe-land',
    'Countries' : 'switzerland-land',
}

for fi in fl:
	rs = RegionSet.objects.create()
	if 'continents' in fi or 'countries' in fi:
		rs.name = os.path.basename(fi).split(".")[0].capitalize()
	else:
		rs.name = os.path.basename(fi).split(".")[0].upper()

	rs.long_name = longNames[rs.name]
	rs.primary_region_key = primaryRegions[rs.name]

	with open(fi) as f:
		gj = geojson.load(f)

	features = gj.features
	feature = features[0]

	for feature in features:
		if 'LAB' in feature['properties']:
			label = feature['properties']['LAB']
		elif 'NUTS_ID' in feature['properties']:
			label = feature['properties']['NUTS_ID']
		elif 'name' in feature['properties']:
			label = feature['properties']['name']
		elif 'short_name' in feature['properties']:
			label = feature['properties']['short_name']

		if not "*" in label:
			if rs.name!="Countries":
				r = Region.objects.create()
				r.land_only = True		
				r.name = label
				if 'NAME' in feature['properties']:
					r.long_name = feature['properties']['NAME']
				elif 'NUTS_ID' in feature['properties']:
					r.long_name = feature['properties']['NUTS_ID']
				elif 'name' in feature['properties']:
					r.long_name = feature['properties']['name']
				elif 'long_name' in feature['properties']:
					r.long_name = feature['properties']['long_name']
				if 'NUMBER' in feature['properties']:
					r.number = feature['properties']['NUMBER']
				elif 'cartodb_id' in feature['properties']:
					r.number = feature['properties']['cartodb_id']
				elif 'id' in feature['properties']:
					r.number = feature['properties']['id']
				r.region_set = rs

			if len(feature['geometry']['coordinates'])>1:
				multipolygon = asShape(feature['geometry'])
				if rs.name=="Continents":
					multipolygonBuffered = multipolygon.buffer(1.1)
				else:
					multipolygonBuffered = multipolygon.buffer(0)
					
				if str(type(multipolygonBuffered)) != "<class 'shapely.geometry.polygon.Polygon'>":
					areas = np.array([c.area for c in multipolygonBuffered])
					if rs.name=="Continents":
						multipolygonInd = areas.index(max(areas))
						r.boundaries = GeosPolygon(list(multipolygonBuffered[multipolygonInd].exterior.coords))

					elif rs.name=="Countries":					
						order = np.argsort(areas)
						if len(areas[areas>=1]) > 1:
							ai = 1
						else:
							ai = ""
						for i, area in enumerate(areas):
							if area >= 1:
								r = Region.objects.create()
								r.land_only = True
								r.name = label + " " + str(ai)
								r.long_name = feature['properties']['name'] + " " + str(ai)
								r.region_set = rs
								r.boundaries = GeosPolygon(list(multipolygonBuffered[i].exterior.coords))
								r.full_clean()
								r.save()
								if str(type(ai)) != "<class 'str'>":
									ai = ai+1

				else:
					if rs.name=="Countries":
						r = Region.objects.create()
						r.land_only = True
						r.name = label
						r.long_name = feature['properties']['name']
						r.region_set = rs
						r.boundaries = GeosPolygon(list(multipolygonBuffered.exterior.coords))
						r.full_clean()
						r.save()
					else:
						r.boundaries = GeosPolygon(list(multipolygonBuffered.exterior.coords))

			else:				
				if rs.name=="Countries":
					r = Region.objects.create()
					r.land_only = True
					r.name = label
					r.long_name = feature['properties']['name']
					r.region_set = rs
					r.boundaries = GeosPolygon(feature['geometry']['coordinates'][0])
					r.full_clean()
					r.save()

				else:
					r.boundaries = GeosPolygon(feature['geometry']['coordinates'][0])

			if rs.name!="Countries":
				r.full_clean()
				r.save()

	rs.full_clean()
	rs.save()