from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins, viewsets
from django_filters import rest_framework as filters
from main.models import IndicatorGroup, IndicatorAggregate, Indicator, RegionSet, Region, Plot, PlotType
from main.serializers import IndicatorGroupSerializer, IndicatorSerializer, IndicatorAggregateSerializer, RegionSerializer, RegionSetSerializer, PlotSerializer, PlotTypeSerializer


class CreateRetrieveUpdateViewSet(mixins.CreateModelMixin,
                                  mixins.RetrieveModelMixin,
                                  mixins.UpdateModelMixin,
                                  viewsets.GenericViewSet):
    """
    A viewset that provides `create`, `update`, and `retrieve` actions.
 
    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.
    """
    pass


class ListRetrieveViewSet(mixins.ListModelMixin,mixins.RetrieveModelMixin,viewsets.GenericViewSet):
    """
    A viewset that provides `list` actions.
 
    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.
    """
    pass


class PlotTypeViewSet(ListRetrieveViewSet):
    queryset = PlotType.objects.all().order_by('order_id')
    serializer_class = PlotTypeSerializer
    lookup_field = 'key'


class IndicatorGroupViewSet(ListRetrieveViewSet):
    queryset = IndicatorGroup.objects.all().order_by('order_id')
    serializer_class = IndicatorGroupSerializer
    lookup_field = 'key'


class IndicatorAggregateViewSet(ListRetrieveViewSet):
    queryset = IndicatorAggregate.objects.all().order_by('order_id')
    serializer_class = IndicatorAggregateSerializer
    lookup_field = 'key'
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('indicator_group','indicator_group__key')


class IndicatorViewSet(ListRetrieveViewSet):
    queryset = Indicator.objects.all().order_by('name')
    serializer_class = IndicatorSerializer
    lookup_field = 'key'
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('indicator_aggregate','indicator_aggregate__key')


class RegionViewSet(ListRetrieveViewSet):
    queryset = Region.objects.all().order_by('region_set')
    serializer_class = RegionSerializer
    lookup_field = 'key'
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('region_set',)


class RegionSetViewSet(ListRetrieveViewSet):
    queryset = RegionSet.objects.all().order_by('name')
    serializer_class = RegionSetSerializer
    lookup_field = 'key'


class PlotViewSet(CreateRetrieveUpdateViewSet):
    """
    Viewset providing access to read pre-generated plot data or to store new plot data in the database
    """
    queryset = Plot.objects.all()
    serializer_class = PlotSerializer
    lookup_field = 'key'

    def perform_create(self, serializer):
        serializer.save()
