from main.utils import ChoiceEnum

class IndicatorTypeChoice(ChoiceEnum):
    diagnostic = 'Diagnostic estimate'
    index = 'Climate index'
    variable = 'Climate variable'


class DataTypeChoice(ChoiceEnum):
    num = 'numeric'
    cat = 'categorical'


class PlotTypeChoice(ChoiceEnum):
    map = 'Maps'    
    reg = 'Regional Timeseries'
    regavg = 'Regional Averages'
    hovlat = 'Hovmoller (time-latitude)'
    hovlon = 'Hovmoller (time-longitude)'
    projatl = 'Projection Atlas'

class StatisticChoice(ChoiceEnum):
    avg = 'average'
    med = 'median'
    std = 'standard deviation'
    min = 'minimum'
    max = 'maximum'
    mode = 'mode'
    lower = 'lower percentile (Q25)'
    upper = 'upper percentile (Q75)'


class RegionTypeChoice(ChoiceEnum): # to be deleted!
    srex = 'SREX regions'
    srexext = 'SREX regions (extended)'
    prudence = 'PRUDENCE regions'
    global_ = 'Global regions'
    continent = 'Continental regions'
    lat_band = 'Latitude bands'
    lon_band = 'Longitude bands'

  
class SpatialResolutionChoice(ChoiceEnum): # to be deleted!
    pointzerofive = '0.05 degrees'
    pointone = '0.1 degrees'
    onequarter = '0.25 degrees'
    half = '0.5 degrees'
    twoquarters = '0.75 degrees'
    one = '1 degree'
    oneandahalf = '1.5 degrees'
    two = '2 degrees'
    twoandahalf = '2.5 degrees'
    five = '5 degrees'
    equalarea = 'equal area resolution'
    irregular = 'irregular resolution'
    point = '- (point data)'


class TimeResolutionChoice(ChoiceEnum): # to be deleted!
     decadal = 'Decadal resolution'
     annual = 'Annual resolution'
     seasonal = 'Seasonal resolution'
     monthly = 'Monthly resolution'
     daily = 'Daily resolution'
     hourly = 'Hourly resolution'
     single = 'Single time step'
     time_average = 'Time average'