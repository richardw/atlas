# Django
from django.db import IntegrityError, transaction
from django.contrib.gis import admin
from django.contrib import messages

# local Django
from main.fields import ArrayChoiceField
from main.forms import RegionAdminForm, IndicatorAdminForm
from main.models import Reference, RegionSet, Region, Indicator, IndicatorGroup, IndicatorAggregate, Plot, PlotType, Statistic


@admin.register(Statistic)
class StatisticAdmin(admin.ModelAdmin):
    search_fields = ['name']
    readonly_fields = ['key']
    ordering = ['name']
    save_as = True
    save_on_top = True


@admin.register(PlotType)
class PlotTypeAdmin(admin.ModelAdmin):
    search_fields = ['name']
    readonly_fields = ['key']
    ordering = ['order_id']
    save_as = True
    save_on_top = True


@admin.register(Reference)
class ReferenceAdmin(admin.ModelAdmin):
    ordering = ['created_at']
    search_fields = ['title']
    autocomplete_fields = ['indicator_aggregate']
    save_as = True
    save_on_top = True


@admin.register(RegionSet)
class RegionSetAdmin(admin.ModelAdmin):
    ordering = ['created_at']
    search_fields = ['region_type']
    readonly_fields = ['key']
    save_as = True
    save_on_top = True

  
@admin.register(Region)
class RegionAdmin(admin.GeoModelAdmin):
    ordering = ['created_at']
    search_fields = ['name']
    readonly_fields = ['key']
    save_as = True
    save_on_top = True
    form = RegionAdminForm


@admin.register(IndicatorAggregate)
class IndicatorAggregateAdmin(admin.ModelAdmin):    
    ordering = ['name']
    search_fields = ['name','key']
    readonly_fields = ['key']
    list_display = ('key', 'name', 'primary_indicator_key', 'primary_statistic_key', 'indicator_group', 'use_custom_time_dimension', 'order_id', 'created_at')
    save_as = True
    save_on_top = True


@admin.register(Indicator)
class IndicatorAdmin(admin.ModelAdmin): 
    ordering = ['key']
    search_fields = ['name','key','filename']
    readonly_fields = ['key']
    list_display = ('key', 'filename', 'indicator_group', 'indicator_aggregate', 'statistic_default', 'number_of_plots', 'created_at')
    save_as = True
    save_on_top = True
    form = IndicatorAdminForm


@admin.register(IndicatorGroup)
class IndicatorGroupAdmin(admin.ModelAdmin):    
    ordering = ['created_at']
    search_fields = ['name']
    readonly_fields = ['key']
    save_as = True
    save_on_top = True


@admin.register(Plot)
class PlotAdmin(admin.ModelAdmin):    
    ordering = ['created_at']
    readonly_fields = ['key']
    autocomplete_fields = ['indicator','region']
    save_as = True
    save_on_top = True
    
    def save_model(self, request, obj, form, change):
        try:
            with transaction.atomic():
                obj.save()
        except IntegrityError as e:
            messages.set_level(request, messages.ERROR)
            messages.add_message(request, messages.ERROR, 'Operation interrupted! You were trying to save a plot that already exists. Error message: "'+str(e.__cause__)+'"', fail_silently=False, extra_tags='safe')