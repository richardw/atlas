from django.core.exceptions import ValidationError
from django.utils.text import slugify
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers, status
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework.relations import HyperlinkedRelatedField
from rest_framework.response import Response

from main.fields import ChoiceDisplayField
from main.models import Plot, Indicator, IndicatorGroup, IndicatorAggregate, Reference, RegionSet, Region, PlotType, Statistic
from main.derive_plot_data import CreatePlot


class DefaultModelSerializer(serializers.ModelSerializer):
    serializer_choice_field = ChoiceDisplayField


class ReferenceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reference
        fields = "__all__"


class RegionSetSerializer(DefaultModelSerializer):

    class Meta:
        model = RegionSet
        fields = ("id","key","name","long_name","primary_region_key","primary_region_id")
        lookup_field = 'name'


class RegionSerializer(GeoFeatureModelSerializer):
    region_set = RegionSetSerializer(many=False)

    class Meta:
        model = Region
        fields = "__all__"
        lookup_field = 'key'
        geo_field = 'boundaries'
        id_field = 'key'


class StatisticSerializer(DefaultModelSerializer):
    
    class Meta:
        model = Statistic
        fields = "__all__"
        lookup_field = 'key'


class PlotTypeSerializer(DefaultModelSerializer):

    class Meta:
        model = PlotType
        fields = "__all__"        
        lookup_field = 'key'


class IndicatorGroupSerializer(DefaultModelSerializer):
    image_url = serializers.SerializerMethodField()

    class Meta:
        model = IndicatorGroup
        fields = "__all__"
        lookup_field = 'key'

    def get_image_url(self, indicatorgroup):
        request = self.context.get('request')
        image_url = indicatorgroup.image.url
        return request.build_absolute_uri(image_url)


class IndicatorAggregateSerializer(DefaultModelSerializer):
    indicator_group = IndicatorGroupSerializer(many=False)
    references = ReferenceSerializer(many=True)
    
    class Meta:
        model = IndicatorAggregate
        fields = "__all__"
        lookup_field = 'key'


class IndicatorSerializer(DefaultModelSerializer):
    indicator_group = IndicatorGroupSerializer(many=False)
    indicator_aggregate = IndicatorAggregateSerializer(many=False)
    plot_types_available = PlotTypeSerializer(many=True)
    statistics_available = StatisticSerializer(many=True)
    statistic_default = StatisticSerializer()
    
    class Meta:
        model = Indicator
        fields = "__all__"
        lookup_field = 'key'


class PlotSerializer(DefaultModelSerializer):
    def create(self, validated_data):
        validated_data['key'] = slugify(
            str(validated_data['indicator'].key) + "_" +
            str(validated_data['region'].key) + "_" +
            str(validated_data['plot_type']) + "_" +
            str(validated_data['statistic']))

        plot = Plot.objects.filter(key=validated_data['key'])
        if len(plot)>=1:
            return Plot.objects.get(key=validated_data['key'])
        else:
            plot_data_all = CreatePlot(
                indicator = validated_data['indicator'],
                plot_type = validated_data['plot_type'],
                statistic = validated_data['statistic'],
                region = validated_data['region'],
                filename = validated_data['indicator'].filename
            ).get_data()

            validated_data['plot_data_1d'] = plot_data_all[0]
            validated_data['plot_data_2d'] = plot_data_all[1]
            validated_data['plot_data_lats'] = plot_data_all[2]            
            validated_data['plot_data_lons'] = plot_data_all[3]
            validated_data['xaxis_tickvals'] = plot_data_all[4]
            validated_data['xaxis_ticktext'] = plot_data_all[5]
            validated_data['yaxis_tickvals'] = plot_data_all[6]
            validated_data['yaxis_ticktext'] = plot_data_all[7]

            if validated_data['indicator'].indicator_group.key != "past": # only save the plot for later if the corresponding indicator is not updated frequently
                return Plot.objects.create(**validated_data)
            else:
                return validated_data

    class Meta:
        model = Plot
        fields = ("indicator", "region", "plot_type", "statistic", "plot_data_1d", "plot_data_2d", "plot_data_lats", "plot_data_lons", "xaxis_tickvals", "xaxis_ticktext", "yaxis_tickvals", "yaxis_ticktext")
        lookup_field = 'key'
        extra_kwargs = {
            'url': {'lookup_field': 'key'}
        }
