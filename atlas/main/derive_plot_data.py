from django.core.exceptions import ValidationError

import ast
import glob
from netCDF4 import Dataset
import numpy as np
import os
import regionmask
import xarray as xr


class CreatePlot(object):
	"""Return a Plotly-compatible array representation of the plot data object, based on a number of request arguments
	"""

	def __init__(self, **kwargs):
		"""Initialize the class using a number of keyword arguments
		Keyword arguments passed to this class are made accessible via a python dict

		Args:
			indicator (str): Identifier of the requested indicator
			plot_type (str): Identifier of the requested plot type
			statistic (str): Identifier of the requested statistic
			region (str): Identifier of the requested region
			data_path (str): Path to where the input (NetCDF) data is stored
		"""
		self.__dict__.update(kwargs)
		self.plot_data_1d = []
		self.plot_data_2d = []
		self.plot_data_lons = []
		self.plot_data_lats = []
		self.xaxis_tickvals = []
		self.xaxis_ticktext = []
		self.yaxis_tickvals = []
		self.yaxis_ticktext = []

	def read_data(self):
		"""Read subset of NetCDF file(s) as xarray and extract subset if necessary
		"""
		if not self.filename in ["undefined","tbd"]:
			data_path = os.environ["GEOSERVER_DATA_DIR"]
			data_path = os.path.join(data_path,"netcdf")
			filename = [os.path.join(data_path, self.filename)]
			vars_to_drop = ["lat_bnds","lon_bnds"]
			
			if len(filename) == 1:			
				x = xr.open_dataset(filename[0], drop_variables=vars_to_drop, decode_cf=False)
				x[self.indicator.netcdf_variable_name].attrs.pop('units',None)
				x = xr.decode_cf(x)
			elif len(filename) > 1:
				raise ValidationError("Multiple NetCDF files found - currently not supported!")
			else:
				raise ValidationError("Requested NetCDF file(s) not found!")

			x = x[self.indicator.netcdf_variable_name] # make x an xarray.DataArray of the requested variable

			if self.region.region_set != "global_":
				x_boundaries = [list(elem) for elem in self.region.boundaries[0][:]][0:(len(self.region.boundaries[0][:])-1)]
				x_lons = slice(min([xb[0] for xb in x_boundaries]), max([xb[0] for xb in x_boundaries]))
				x_lats = slice(min([xb[1] for xb in x_boundaries]), max([xb[1] for xb in x_boundaries]))
				x = x.sel(lat=x_lats, lon=x_lons) # get requested lat-lon slice
				if len(x.lon) >= 1 and len(x.lat) >= 1:
					region_boundaries = regionmask.Regions_cls("region_boundaries", [0], [self.region.long_name], [self.region.name], [x_boundaries])
					mask = region_boundaries.mask(x.lon[:], x.lat[:], wrap_lon=False)
					x = x.where(mask == 0) # mask out everything outside the polygon (only relevant if polygon_mask does not represent a rectangular region)
				else:
					x = None # region too small to contain any grid point

			# if self.region.land_only:
			# 	land_mask = regionmask.defined_regions.natural_earth.land_110 # use the same land mask for all requests
			# 	mask = land_mask.mask(x.lon[:], x.lat[:], wrap_lon=False)
			# 	x = x.where(mask == 0) # mask out the ocean

			return x

		else:
			return None

	def prepare_data(self):
		x = self.read_data()

		if x is not None:
			apply_over_lookup = {
				'regavg': ['lon','lat'],
				'reg': ['lon','lat'],
				'hovlon': ['lat'],
				'hovlat': ['lon']
			}
			apply_over = apply_over_lookup[self.plot_type]

			if "time" in x.dims:
				ntime = len(x.coords["time"])
			elif "obs" in x.dims:
				ntime = len(x.coords["obs"])
			elif "dtglob" in x.dims:
				ntime = len(x.coords["dtglob"])

			nlon = len(x.coords["lon"])
			nlat = len(x.coords["lat"])
			ncell = nlat * nlon
			# print("ntime "+str(ntime)+" nlat "+str(nlat)+" nlon "+str(nlon)+" ncell "+str(ncell))

			if self.indicator.indicator_aggregate.data_type == "cat":
				if "mapping" in x.attrs.keys():
					mapping = ast.literal_eval("{"+x.mapping+"}")
					self.yaxis_tickvals = list(map(int, mapping.keys()))
					self.yaxis_ticktext = list(mapping.values())				
					if "np.nan" in self.yaxis_ticktext:
						ind_remove = self.yaxis_ticktext.index("np.nan")
						del self.yaxis_tickvals[ind_remove]
						del self.yaxis_ticktext[ind_remove]
					nclasses = len(self.yaxis_tickvals)
				else:
					raise ValidationError("Categorical data must contain a mapping attribute that allows \
						to match the data in the NetCDF file (numeric) \
						to classes (type String). E.g., :mapping = \
						\"\'0\':\'-\',\'1\':\'a (70%)\',\'2\':\'a (95%)\'\"")

			if self.statistic == "avg":
				xout = x.mean(dim=apply_over, skipna=True)
			elif self.statistic == "med":
				xout = x.median(dim=apply_over, skipna=True)
			elif self.statistic == "std":
				xout = x.std(dim=apply_over, skipna=True)
			elif self.statistic == "min":
				xout = x.min(dim=apply_over, skipna=True)
			elif self.statistic == "max":
				xout = x.max(dim=apply_over, skipna=True)
			elif self.statistic == "lower":
				xout = x.quantile(q=0.25,dim=apply_over)
			elif self.statistic == "upper":
				xout = x.quantile(q=0.75,dim=apply_over)
			elif self.statistic == "mode":
				if apply_over == ['lon','lat']:
					xtemp = np.zeros(shape=[nclasses,ntime], dtype=int)
				elif apply_over == ['lon']:
					xtemp = np.zeros(shape=[nclasses,ntime,nlat], dtype=int)
				elif apply_over == ['lat']:
					xtemp = np.zeros(shape=[nclasses,ntime,nlon], dtype=int)
				
				x = x.where(x, -9999) # replace the missing values with a value that is not in self.yaxis_tickvals				
				for i in range(nclasses):
					if apply_over == ['lon','lat']:
						xtemp[i] = x.where(x==self.yaxis_tickvals[i]).count(axis=(1,2))
					elif apply_over == ['lon']:
						xtemp[i] = x.where(x==self.yaxis_tickvals[i]).count(axis=(2))
					elif apply_over == ['lat']:
						xtemp[i] = x.where(x==self.yaxis_tickvals[i]).count(axis=(1))

				xtemp = xr.DataArray(xtemp).argmax('dim_0')
				if apply_over == ['lon'] or apply_over == ['lat']:
				 	xtemp = xtemp.stack(z=("dim_1","dim_2"))
				
				xout = np.array(xtemp)
				if apply_over == ['lon']:
				 	xout = xout.reshape([ntime, nlat])
				elif apply_over == ['lat']:
				 	xout = xout.reshape([ntime, nlon])

			if str(type(xout)) == "<class 'xarray.core.dataarray.DataArray'>":
				xout = np.nan_to_num(xout.values)

			if len(xout.shape) == 1:
				self.plot_data_1d = xout
			elif len(xout.shape) == 2:
				self.plot_data_2d = xout

			if self.plot_type == "hovlon":
				self.plot_data_lons = x.lon
			elif self.plot_type == "hovlat":
				self.plot_data_lats = x.lat				
		
		else:
			self.plot_data_1d = [-9999]

	def get_data(self):
		self.prepare_data()
		return [self.plot_data_1d, self.plot_data_2d, self.plot_data_lats, self.plot_data_lons, self.xaxis_tickvals, self.xaxis_ticktext, self.yaxis_tickvals, self.yaxis_ticktext]
