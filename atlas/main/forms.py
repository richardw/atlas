# Django
from django import forms
from django.contrib.gis.geos import Polygon
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

# local Django
from main.models import Region, Indicator, IndicatorGroup, IndicatorAggregate
from shutil import which


class IndicatorAggregateAdminForm(forms.ModelForm):
    class Meta:
        model = IndicatorAggregate
        fields = '__all__'

    def clean(self):
        indicator = self.cleaned_data.get('indicator')
        if indicator:
            self.indicator_group = indicator.indicator_group

        statistic_default = self.cleaned_data.get('statistic_default')
        statistics_available = self.cleaned_data.get('statistics_available')
        if statistic_default:
            if not statistic_default in statistics_available:
                raise ValidationError("The default statistic '%s' is not among the statistics available for this indicator." % str(statistic_default))

        return self.cleaned_data


class IndicatorGroupAdminForm(forms.ModelForm):
    class Meta:
        model = IndicatorGroup
        fields = '__all__'

    def clean(self):
        indicator = Indicator.objects.filter(key = self.cleaned_data.get('primary_indicator_key'))
        if not indicator:
            raise ValidationError("The chosen primary indicator does not exist.")

        indicator_aggregate = IndicatorAggregate.objects.filter(key = self.cleaned_data.get('primary_indicator_aggregate_key'))
        if not indicator_aggregate:
            raise ValidationError("The chosen primary indicator aggregate does not exist.")


class IndicatorAdminForm(forms.ModelForm):
    class Meta:
        model = Indicator
        fields = '__all__'

    def clean(self):
        return self.cleaned_data

class RegionAdminForm(forms.ModelForm):

    latitude_region_boundaries_0 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    longitude_region_boundaries_0 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,
    )    
    latitude_region_boundaries_1 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    longitude_region_boundaries_1 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    latitude_region_boundaries_2 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    longitude_region_boundaries_2 = forms.FloatField(
        min_value = -180,
        max_value = 260,
        required = False,        
    )
    latitude_region_boundaries_3 = forms.FloatField(
        min_value = -180,
        max_value = 260,
        required = False,        
    )
    longitude_region_boundaries_3 = forms.FloatField(
        min_value = -180,
        max_value = 260,
        required = False,        
    )
    latitude_region_boundaries_4 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    longitude_region_boundaries_4 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    latitude_region_boundaries_5 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    longitude_region_boundaries_5 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    latitude_region_boundaries_6 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    longitude_region_boundaries_6 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    latitude_region_boundaries_7 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    longitude_region_boundaries_7 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    latitude_region_boundaries_8 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    longitude_region_boundaries_8 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    latitude_region_boundaries_9 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )
    longitude_region_boundaries_9 = forms.FloatField(
        min_value = -180,
        max_value = 180,
        required = False,        
    )

    class Meta(object):
        model = Region
        fields = '__all__'
        widgets = {'boundaries': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        coordinates = self.initial.get('boundaries', None)
        if isinstance(coordinates, Polygon):
            for ind in range(0,(len(coordinates.tuple[0])-1)):
                self.initial['longitude_region_boundaries_{index}'.format(index=ind)], self.initial['latitude_region_boundaries_{index}'.format(index=ind)] = coordinates.tuple[0][ind]

    def clean(self):
        data = super().clean()       
        lonlat_boundaries = []
        ind = 0
        while ind < 10:
            if data['longitude_region_boundaries_'+str(ind)] and data['latitude_region_boundaries_'+str(ind)]:
                if data['latitude_region_boundaries_'+str(ind)] == 90:
                    data['latitude_region_boundaries_'+str(ind)] = 90 - 0.0001 # subtract a small number from regions that start at 90°N to get rid of the following error: "Antipodal (180 degrees long) edge detected!"
                lonlat_boundaries.append((data['longitude_region_boundaries_'+str(ind)], data['latitude_region_boundaries_'+str(ind)]))
            ind += 1
        
        map_boundaries = data.get('boundaries')
        if len(lonlat_boundaries) > 0 and len(lonlat_boundaries) <= 2:
            raise ValidationError(_("You have to provide at least three lat/lon coordinate pairs (boundary coordinates) to define a region."))
        elif len(lonlat_boundaries) > 2 and not map_boundaries:            
            lonlat_boundaries.append((data['longitude_region_boundaries_0'], data['latitude_region_boundaries_0']))                    
            data['boundaries'] = Polygon(lonlat_boundaries) # override the boundaries Polygon
        elif map_boundaries and len(lonlat_boundaries) == 0:
            ind = 0
            while ind < len(map_boundaries.tuple):
                data['longitude_region_boundaries_'+str(ind)] = map_boundaries.tuple[0][ind][0]
                data['latitude_region_boundaries_'+str(ind)] = map_boundaries.tuple[0][ind][1]
                ind += 1
        elif map_boundaries and len(lonlat_boundaries) > 2:
            raise ValidationError(_("You have provided both lat/lon coordinates in both the text fields and in the map. Delete one of them to make clear which values to use. I.e., if you have made manual changes to the lat/lon values, delete the polygon on the map first."))
            
        return data