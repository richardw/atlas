# Django
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models import Count
from django.utils.timezone import datetime
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify

# external
import logging
import os
from psycopg2.extras import DateTimeTZRange
from atlas_backend.settings import STATIC_ROOT

# local Django
from django.contrib.postgres.fields import ArrayField
from main.fields import NumpyArrayField
from main.choices import DataTypeChoice, PlotTypeChoice, StatisticChoice, IndicatorTypeChoice


logger = logging.getLogger(__name__)


def get_plot(id):
    try:
        return Plot.objects.get(pk=id)
    except Plot.DoesNotExist:
        return False


class Reference(models.Model):
    """
    Model to hold all information related to a literature reference.
    
    Relations:
    m:1 (1 indicator is associated with 0, 1 or multiple references).
    
    Field Description:
    author_last_name: Given name of the first author of the literature reference.
    author_first_name: First name of the first author of the literature reference.
    year: Year of the literature reference.
    title: Title of the literature reference.
    doi: DOI of the literature reference.
    url: URL of the literature reference (automatically created from the provided DOI).
    indicator: Indicator associated with the literature reference.
    created_at: date at which this entity was created.
    updated_at: date and time at which this entity was updated.
    """
    author_last_name = models.CharField("Author (last name)", max_length=50, blank=False, null=True, help_text="Given name of first author.")
    author_first_name = models.CharField("Author (first name)", max_length=50, blank=False, null=True, help_text="First name of first author.")
    coauthors = models.CharField("Coauthors", max_length=500, blank=False, null=True, help_text="List of coauthors ([Last name], [initial of first name]., *)")
    year = models.PositiveIntegerField("Year", validators=[
        MinValueValidator(1900, "The year of publication must be within the range [1900, "+str(datetime.now().year)+"]"),
        MaxValueValidator(datetime.now().year, "The year of publication must be within the range [1900, "+str(datetime.now().year)+"]"),
        ], blank=False, null=True, help_text="Year at which this literature reference has been published (format: <YYYY>).")    
    title = models.TextField("Title of publication", blank=False, null=True, help_text="Title of the literature reference.")
    journal = models.CharField("Journal", max_length=100, blank=False, null=True, help_text="Name of journal.")
    volume_issue_pages = models.CharField("Volume, issue and pages", max_length=100, blank=False, null=True, help_text="Volume, issue and page information required to identify the article.")
    doi = models.CharField("DOI", max_length=100, blank=False, null=True, help_text="DOI (Digital Object Identifier) of the literature reference.")
    url = models.URLField("URL", editable=False, blank=False, null=True, help_text="URL pointing to dx.doi.org/{DOI}.")
    indicator_aggregate = models.ForeignKey("IndicatorAggregate", related_name='references', on_delete=models.PROTECT, null=False, help_text="Indicator aggregate that this literature reference is associated with.") 
    
    created_at = models.DateField(editable=False, auto_now_add=True)
    updated_at = models.DateTimeField(editable=False, auto_now=True)

    class Meta:
        verbose_name_plural = "references"
    
    def __str__(self):
        return u"%s, %s (%s): %s" % (self.author_last_name, self.author_first_name.capitalize()[0], str(self.year), self.title)    

    def save(self):
        if self.doi:
            self.url = "http://dx.doi.org/"+self.doi
        super(Reference, self).save()


class RegionSet(models.Model):
    """
    Model to hold all information related to a region set.

    Field Description:
    name: The short name (abbreviation) of the region.
    long_name: The verbose name of the region.
    primary_region_key: The region that is associated with this region set as the primary region
    """    
    key = models.SlugField(null=False, blank=False, unique=True, editable=False, max_length=999, help_text="Unique (auto-generated) slug identifying this region set.")    
    name = models.CharField('Short name', null=False, blank=False, unique=True, max_length=999, help_text="Type (origin of definition) of region.")
    long_name = models.CharField('Long name', null=False, blank=False, unique=False, max_length=999, help_text="Description of the region type.")
    primary_region_key = models.SlugField(null=False, blank=True, unique=False, max_length=999, help_text="Key of the primary region associated with the chosen region type. The primary region corresponds to the region that is shown first when the user selects a region set.")
    primary_region_id = models.PositiveIntegerField(null=False, blank=True, unique=False, default=0, editable=False, help_text="ID of the primary region associated with the chosen region type. The primary region corresponds to the region that is shown first when the user selects a region set.")
    created_at = models.DateField(editable=False, auto_now_add=True)
    updated_at = models.DateTimeField(editable=False, auto_now=True)

    class Meta:
        verbose_name_plural = "region sets"
    
    def __str__(self):
        if self.primary_region_key:
            return u"%s (primary region: %s)" % (self.name, self.primary_region_key)
        else:
            return u"%s" % (self.name)

    def clean(self):
        if (self.primary_region_key != ""):
            if (not Region.objects.filter(region_key = self.primary_region_key)):
                raise ValidationError(_("The chosen primary region must be either a valid region or an empty string."))
            else:
                self.primary_region_id = Region.objects.filter(region_key = self.primary_region_key)[0].id

        self.key = slugify(u"%s" % (self.name))


class Region(models.Model):
    """
    Model to hold all information related to a region that indicators are related to.
    
    Relations:
    m:1 (1 region set is associated with 0, 1 or multiple regions)    

    Field Description:
    name: The unique (indexed) short name (identifier) of the region.
    long_name: The full name of the region.
    number: Official number of the region (if available).
    region_set: The region set associated with this region.    
    land_only: Does this region represent land-only gridcells (TRUE) or both land and ocean (FALSE)?
    boundaries: The boundaries (coordinate pairs) of the region.  Cannot be used in conjunction with the field 'point'.
    created_at: date at which this entity was created.
    updated_at: date and time at which this entity was updated.
    """
    key = models.SlugField(null=False, blank=False, unique=True, editable=False, max_length=999, help_text="Unique (auto-generated) slug identifying this region.")
    region_key = models.SlugField(null=False, blank=False, unique=True, editable=False, max_length=999, help_text="Auto-generated slug identifying this region.")
    name = models.CharField('Name', max_length=100, unique=False, blank=False, null=False, default="", help_text="Short name of the region.")
    long_name = models.CharField('Long name', max_length=200, null=False, blank=True, help_text="Full name of the region.")
    number = models.PositiveIntegerField('Region number', null=True, blank=True, help_text="Official number of the region")
    region_set = models.ForeignKey("RegionSet", related_name='regions', null=True, on_delete=models.PROTECT, help_text="Region set that this region is associated with.")
    land_only = models.BooleanField('Land only', null=False, blank=False, default=False, help_text="Does this region represent land grid cells only?")
    boundaries = models.PolygonField('Region boundaries', srid=4326, dim=2, null=True, blank=True, geography=True, help_text="Coordinate pairs (longitude, latitude) of the region's boundaries.")

    created_at = models.DateField(editable=False, auto_now_add=True)
    updated_at = models.DateTimeField(editable=False, auto_now=True)

    class Meta:
        verbose_name_plural = "regions"
    
    def __str__(self):
        if self.land_only:
            return u"%s (%s, land only)" % (self.name, self.region_set)
        else:
            return u"%s (%s)" % (self.name, self.region_set)
    
    def clean(self):
        if not self.boundaries:
            raise ValidationError(_("Please specify boundary coordinates for this region."))

        if self.land_only:
            self.key = slugify(u"%s-%s-land" % (self.region_set.name, self.name))
            self.region_key = slugify(u"%s-land" % (self.name))
        else:
            self.key = slugify(u"%s-%s" % (self.region_set.name, self.name))
            self.region_key = slugify(u"%s" % (self.name))


class PlotType(models.Model):
    key = models.CharField(max_length=10, editable=False, unique=True) # used to identify the plot type (auto-generated)
    name = models.CharField(max_length=50) # used to identify the name of the plot type
    icon = models.CharField(max_length=50) # used to identify the FontAwesome icon
    control_component_name = models.CharField(max_length=50, default="plotcontrol") # used to identify the Vue control component
    component_name = models.CharField(max_length=50) # used to identify the Vue component    
    order_id = models.PositiveIntegerField(null=True, blank=True, unique=False, help_text="Number (positive integer) defining the order in which the available plot types are exposed in the front-end.")

    def clean(self):
        if not self.name in [name for key,name in PlotTypeChoice.choices()]:
            raise ValidationError("Invalid plot type provided. Add an entry to the PlotTypeChoice class prior to adding a new plot type.")
        else:
            ind=[c[1] for c in PlotTypeChoice.choices()].index(self.name)
            self.key = PlotTypeChoice.choices()[ind][0]
            if not self.icon:
                self.icon = self.key

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["order_id"]


class Statistic(models.Model):
    name = models.CharField(max_length=50, unique=True)
    key = models.CharField(max_length=10, unique=True, editable=False)

    def clean(self):        
        if not self.name in [name for key,name in StatisticChoice.choices()]:
            raise ValidationError("Invalid statistic provided. Add an entry to the StatisticChoice class prior to adding a new statistic.")
        else:
            ind=[c[1] for c in StatisticChoice.choices()].index(self.name)
            self.key = StatisticChoice.choices()[ind][0]

    def __str__(self):
        return self.name


class IndicatorGroup(models.Model):
    """
    Model to hold all information related to an indicator group.
    
    Field Description:
    name: The unique, non-empty identifier of the indicator group.
    headline: A short concise and eye-catching text describing the indicator group.
    description: A longer description of what the indicator group represents.
    image: An image that represents the indicator group. Must be suitable for full-width display.
    primary_indicator_key: The key of the indicator that should be shown first when selecting an indicator group.
    created_at: date at which this entity was created.
    updated_at: date and time at which this entity was updated.
    """
    key = models.SlugField(null=False, blank=False, unique=True, editable=False, max_length=999, help_text="Unique (auto-generated) slug identifying this indicator group.")
    name = models.CharField(verbose_name="name", max_length=50, unique=True, blank=False, null=False, help_text="Name of the indicator group")
    headline = models.CharField(verbose_name="headline", max_length=999, null=False, blank=True, default="", help_text="Very concise, eye catching-words describing the indicator group. To be displayed on the front page.")
    description = models.TextField(verbose_name="description", null=False, blank=True, default="", help_text="A longer description of the indicator group.")
    image = models.ImageField(verbose_name="image", null=False, blank=False, upload_to='', help_text="Image representing the indicator group. To be displayed on the front page.")    
    drilldown_text = models.CharField(verbose_name="Drill down-text", max_length=200, unique=False, blank=True, null=False, default="Please select ...", help_text="Text label displayed on top of the drop-down for the individual indicator aggregates.")
    order_id = models.PositiveIntegerField(null=True, blank=True, unique=False, help_text="Number (positive integer) defining the order at which the indicator groups should appear on the front-end.")
    primary_indicator_aggregate_key = models.SlugField(null=False, blank=True, unique=False, max_length=999, help_text="Name of the primary indicator associated with this indicator group. The primary indicator aggregate corresponding to the indicator that is shown first when the user navigates to one of the indicator groups.")
    primary_indicator_key = models.SlugField(null=False, blank=True, unique=False, max_length=999, help_text="Name of the primary indicator associated with this indicator group. The primary indicator corresponds to the indicator that is shown first when the user navigates to one of the indicator groups.")
    primary_indicator_id = models.PositiveIntegerField(null=False, blank=True, unique=False, editable=False, help_text="ID of the primary indicator associated with this indicator group. The primary indicator corresponds to the indicator that is shown first when the user navigates to one of the indicator groups.")
    primary_statistic_key = models.SlugField(null=False, blank=True, unique=False, max_length=999, help_text="Name of the default statistic associated with the primary indicator associated with this indicator group.")

    created_at = models.DateField(editable=False, auto_now_add=True)
    updated_at = models.DateTimeField(editable=False, auto_now=True)
    
    class Meta:
        verbose_name_plural = "indicator groups"
    
    def __str__(self):
        return u"%s (primary indicator aggregate: %s)" % (self.name, self.primary_indicator_aggregate_key)
    
    def clean(self):
        if (self.primary_indicator_aggregate_key != ""):
            if (not IndicatorAggregate.objects.filter(key = self.primary_indicator_aggregate_key)):
                raise ValidationError(_("The chosen primary indicator aggregate must be either a valid indicator aggregate or an empty string."))

        if (self.primary_indicator_key != ""):
            if (not Indicator.objects.filter(key = self.primary_indicator_key)):
                raise ValidationError(_("The chosen primary indicator must be either a valid indicator or an empty string."))
            else:
                self.primary_indicator_id = Indicator.objects.filter(key = self.primary_indicator_key)[0].id

        self.key = slugify(u"%s" % (self.name))


class IndicatorAggregate(models.Model):
    key = models.SlugField(null=False, blank=False, unique=True, editable=False, max_length=999, help_text="Unique (auto-generated) slug identifying this indicator aggregate. Use this key to name the style attached to the geoserver instance of this indicator!")
    name = models.CharField(verbose_name="Name", max_length=50, unique=True, blank=False, null=False, help_text="Name of the indicator aggregate. This name will be displayed in the indicator drop-down list of the front-end.")
    order_id = models.PositiveIntegerField(unique=False, blank=False, null=True, help_text="Unique ID identifying the order of this indicator aggregate (relevant for listing all indicator aggregates)")
    indicator_group = models.ForeignKey("IndicatorGroup", related_name='indicatoraggregates', null=True, editable=False, on_delete=models.PROTECT, help_text="Indicator group that this indicator is associated with.")
    primary_indicator_key = models.SlugField(null=False, blank=True, unique=False, max_length=999, help_text="Name of the primary indicator associated with this indicator group. The primary indicator corresponds to the indicator that is shown first when the user navigates to one of the indicator groups.")
    primary_indicator_id = models.PositiveIntegerField(null=False, blank=True, unique=False, editable=False, help_text="ID of the primary indicator associated with this indicator group. The primary indicator corresponds to the indicator that is shown first when the user navigates to one of the indicator groups.")
    primary_statistic_key = models.SlugField(null=False, blank=True, unique=False, max_length=999, help_text="Name of the default statistic associated with the primary indicator associated with this indicator aggregate.")
    drilldown_text = models.CharField(verbose_name="Drill down-text", max_length=200, unique=False, blank=True, null=False, default="Please specify ...", help_text="Text label displayed on top of the drop-down for the individual indicators.")    
    use_custom_time_dimension = models.BooleanField(verbose_name="Use a custom 3rd dimension instead of time", default=False, blank=False, null=False, help_text="Replace time axis by another dimension with values provided in 'custom_time_dimension_append' (true) or not (false).")
    use_custom_time_dimension_show_time = models.BooleanField(verbose_name="Display both custom the custom axis AND original dates", default=False, blank=False, null=False, help_text="In addition to showing the custom axis, also show the original time axis values (true) or not (false).")
    custom_time_dimension_prepend = models.CharField(verbose_name="Custom 3rd dimension prepend string", max_length=100, unique=False, blank=True, null=False, default="", help_text="Optional string to prepend to the custom dimension.")
    custom_time_dimension_append = models.CharField(verbose_name="Custom 3rd dimension append string", max_length=100, unique=False, blank=True, null=False, default="", help_text="Optional string to append to the custom dimension.")
    custom_time_dimension_values = ArrayField(models.CharField(null=True, blank=False, default=None, max_length=1000), default=list("0"), help_text="1D-Array representation of alternative x-axis values to be displayed by plotly.js (as a replacement of the default time axis). Must have the same length as the number of time steps in the input file!")       
    date_format = models.CharField(verbose_name="Date format", max_length=100, unique=False, blank=True, default="dS mmmm yyyy", help_text="Date format to be used in the time range slider")
    description = models.TextField(verbose_name="Description", null=False, blank=True, default="", help_text="Short description of the indicator.")
    indicator_type = models.CharField("Indicator type", max_length=50, choices=IndicatorTypeChoice.choices(), default=IndicatorTypeChoice.diagnostic, null=False, blank=False, help_text="Type of indicator.")
    data_type = models.CharField("Data type", max_length=50, choices=DataTypeChoice.choices(), default=DataTypeChoice.num, null=False, blank=False, help_text="Type of data associated with this indicator.")
    attribution = models.CharField("Map attribution", max_length=100, default="copyright by ETH Zürich", blank=False, null=True, help_text="Attribution text for the data set (to be shown at the bottom of the map).")

    created_at = models.DateField(editable=False, auto_now_add=True)
    updated_at = models.DateTimeField(editable=False, auto_now=True)
    
    def clean(self):
        if (self.primary_indicator_key != ""):
            if (not Indicator.objects.filter(key = self.primary_indicator_key)):
                raise ValidationError(_("The chosen primary indicator must be either a valid indicator or an empty string."))
            else:
                self.primary_indicator_id = Indicator.objects.filter(key = self.primary_indicator_key)[0].id
                self.indicator_group = Indicator.objects.filter(key = self.primary_indicator_key)[0].indicator_group


        if self.use_custom_time_dimension_show_time and not self.use_custom_time_dimension:
            raise ValidationError("If you wish to display a custom axis plus the original time information, you also have to tick 'Use a custom 3rd dimension instead of time'!")


        self.key = slugify(u"%s" % (self.name))

    def __str__(self):
        return self.name


class Indicator(models.Model):
    """
    Model to hold all information related to an indicator.
    
    Relations:
    m:1 (1 indicator group is associated with 0, 1 or multiple indicators)
    m:n (0, 1 or multiple plot types are available for 0, 1 or multiple indicators)
    m:n (0, 1 or multiple statistics_available are available for 0, 1 or multiple indicators)
    1:n (1 statistic_default is associated with 0, 1 or multiple indicators)

    Field Description:
    key: The primary key of the indicator.
    name: The unique, non-empty identifier of the indicator. The indicator can either be a diagnostic estimate, a climate index or a climate variable.
    indicator_group: The indicator group associated with this indicator.
    type: The type of the indicator (a diagnostic estimate, a climate index or a climate variable)
    description: A short description of what the indicator is.    
    number_of_plots: Number of plots generated for this indicator.
    created_at: date at which this entity was created.
    updated_at: date and time at which this entity was updated.
    """
    name = models.CharField(verbose_name="Name", max_length=50, unique=False, blank=False, null=False, help_text="Name of the indicator (diagnostic estimate, climate index or climate variable). This name will be displayed in the indicator drop-down list of the front-end.")
    filename = models.CharField(verbose_name="Filename", max_length=100, unique=True, blank=False, null=False, help_text="Name of the NetCDF file within the data source directory $GEOSERVER_DATA_DIR/netcdf/.")    
    netcdf_variable_name = models.CharField(verbose_name="NetCDF variable name", max_length=999, unique=True, blank=False, null=False, help_text="Unique name of the variable in the NetCDF file that holds the associated raster data to be passed via Geoserver. The NetCDF variable name is used to auto-generate a key that is used to (1) access the associated geoserver layer, and (2) access the associated raster style.")
    key = models.SlugField(null=False, blank=False, unique=True, editable=False, max_length=999, help_text="Unique (auto-generated) slug identifying this indicator by use of its NetCDF variable name. Use this key to name the style attached to the geoserver instance of this indicator!")

    indicator_group = models.ForeignKey("IndicatorGroup", related_name='indicators', null=False, on_delete=models.PROTECT, help_text="Indicator group that this indicator is associated with.")
    indicator_aggregate = models.ForeignKey("IndicatorAggregate", related_name='indicators', null=False, on_delete=models.PROTECT, help_text="Indicator aggregate that this indicator is associated with.")

    plot_types_available = models.ManyToManyField("PlotType", verbose_name="Plot types available", help_text="Plot types available (i.e., valid) for this indicator.")
    statistics_available = models.ManyToManyField("Statistic", verbose_name="Statistics available", related_name="statistics_available", help_text="Statistical operations available (i.e., valid) for this indicator.")   
    statistic_default = models.ForeignKey("Statistic", verbose_name="Default statistic", related_name="statistic_default", on_delete=models.PROTECT, help_text="Statistic that is used as a default in for a plot request made with this indicator.")
    default_zoom = models.PositiveIntegerField(verbose_name="Default zoom level", default=2, validators=[
        MinValueValidator(0, "Valid zoom levels must be within the range [0, 20]"),
        MaxValueValidator(20, "Valid zoom levels must be within the range [0, 20]")
        ], blank=False, null=True, help_text="Default zoom level of the map.")
    min_zoom = models.PositiveIntegerField(verbose_name="Minimum zoom level", default=1, validators=[
        MinValueValidator(0, "Valid zoom levels must be within the range [0, 20]"),
        MaxValueValidator(20, "Valid zoom levels must be within the range [0, 20]")
        ], blank=False, null=True, help_text="Minimum zoom level of the map.")
    max_zoom = models.PositiveIntegerField(verbose_name="Maximum zoom level", default=9, validators=[
        MinValueValidator(0, "Valid zoom levels must be within the range [0, 20]"),
        MaxValueValidator(20, "Valid zoom levels must be within the range [0, 20]")
        ], blank=False, null=True, help_text="Maximum zoom level of the map.")
    transparent = models.BooleanField(verbose_name="Transparent", default=True, blank=False, null=False, help_text="Show a transparent map (true) or not (false).")

    number_of_plots = models.PositiveIntegerField(editable=False)
    created_at = models.DateField(editable=False, auto_now_add=True)
    updated_at = models.DateTimeField(editable=False, auto_now=True)

    class Meta:
        verbose_name_plural = "indicators"
        ordering = ["key"]

    def __str__(self):
        return u"%s" % (self.key)
    
    def clean(self):
        self.key = slugify(u"%s" % (self.netcdf_variable_name))

        if self.pk is not None:
            if get_plot(self.pk):
                self.number_of_plots = Plot.objects.filter(indicator = self.pk).count()
            else:
                self.number_of_plots = 0
        else:
            self.number_of_plots = 0


class Plot(models.Model):
    """
    Model to hold all information related to an individual plot.
    
    Relations:
    m:1 (1 indicator is associated with 0, 1 or multiple plots)
    m:1 (1 region is associated with 0, 1 or multiple plots)
    
    Field Description:
    indicator: The indicator associated with the plot.
    plot_type: The type of plot. Available plot types are hard coded.    
    statistic: Statistic that this plot is based on. This is either a statistic applied in the lat-lon (time series plot), lat (Hovmöller time - lon plot), lon (Hovmöller time - lat plot) or time (map plot) domain.
    region: The region associated with the plot.
    plot_data_*:  Array representation of the plot data to be displayed by plotly.js.
    created_at: date at which this entity was created.
    updated_at: date and time at which this entity was updated.
    """
    key = models.SlugField(null=False, blank=False, unique=True, editable=False, max_length=999, help_text="Unique (auto-generated) key identifying this plot.")    
    indicator = models.ForeignKey("Indicator", related_name='plots', on_delete=models.PROTECT, null=False, help_text="Indicator that this plot is associated with.")
    region = models.ForeignKey("Region", related_name='plots', on_delete=models.PROTECT, null=False, help_text="Region that this plot is associated with.")
    plot_type = models.CharField("Plot type", max_length=50, choices=PlotTypeChoice.choices(), default=PlotTypeChoice.reg, null=False, blank=False, help_text="Type of plot.")
    statistic = models.CharField("Statistic", max_length=50, choices=StatisticChoice.choices(), default=StatisticChoice.avg, null=False, blank=False, help_text="Statistic that is applied in either the lat-lon (time series plot), lat (Hovmöller time - lon plot), lon (Hovmöller time - lat plot) or time (map) domain.")

    plot_data_1d = NumpyArrayField(models.FloatField(null=True, blank=False, default=None), default=list, help_text="1D-Array representation of numerical plot data to be displayed by plotly.js")
    plot_data_2d = NumpyArrayField(NumpyArrayField(models.FloatField(null=True, blank=False, default=None)), default=list, help_text="2D-Array representation of numerical plot data to be displayed by plotly.js")
    xaxis_tickvals = NumpyArrayField(models.FloatField(null=True, blank=False, default=None), default=list, help_text="x-axis tick values")
    yaxis_tickvals = NumpyArrayField(models.FloatField(null=True, blank=False, default=None), default=list, help_text="y-axis tick values")
    xaxis_ticktext = NumpyArrayField(models.CharField(null=True, blank=False, default=None, max_length=30), default=list, help_text="x-axis labels to be printed instead of the x-axis values")
    yaxis_ticktext = NumpyArrayField(models.CharField(null=True, blank=False, default=None, max_length=30), default=list, help_text="y-axis labels to be printed instead of the y-axis values")
    plot_data_lons = NumpyArrayField(models.FloatField(null=True, blank=False, default=None), default=list, help_text="1D-Array representation of the longitude axis to be displayed by plotly.js")
    plot_data_lats = NumpyArrayField(models.FloatField(null=True, blank=False, default=None), default=list, help_text="1D-Array representation of the latitude axis to be displayed by plotly.js")

    created_at = models.DateField(editable=False, auto_now_add=True)
    updated_at = models.DateTimeField(editable=False, auto_now=True)
        
    class Meta:
        verbose_name_plural = "plots"
    
    def __str__(self):
        return u"Indicator: %s | Plot type: %s | Statistic: %s | Region: %s" % (self.indicator, self.plot_type, self.statistic, self.region)
    
    def clean(self):
        if not self.statistic in self.indicator.statistics_available.all():
            raise ValidationError("You have specified a statistic that is invalid for this indicator.")

        if not self.plot_type in self.indicator.plot_types_available.all():
            raise ValidationError("You have specified a plot type that is invalid for this indicator.")

        self.key = slugify(str(self.indicator.key) + "_" + str(self.region.key) + "_" + str(self.plot_type) + "_" + str(self.statistic))
    
    def save(self, *args, **kwargs):
        super(Plot, self).save(*args, **kwargs)
        indicator = Indicator.objects.get(pk=self.indicator.pk)
        indicator.number_of_plots = Plot.objects.filter(indicator__in=Indicator.objects.filter(pk=self.indicator.pk)).count()        
        indicator.save()
