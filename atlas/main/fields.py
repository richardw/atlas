import six
from collections import OrderedDict
from django import forms
from django.contrib.postgres.fields import ArrayField
from rest_framework import serializers
try:
    from numpy import array
except ImportError:
    array = list


class ArrayChoiceField(ArrayField):
    """
    A field that allows us to store an array of choices.
     
    Uses Django 2.0's Postgres ArrayField and a MultipleChoiceField for its
    formfield.
    """
 
    def formfield(self, **kwargs):
        defaults = {
            'form_class': forms.MultipleChoiceField,
            'choices': self.base_field.choices,
        }
        defaults.update(kwargs)
        return super(ArrayField, self).formfield(**defaults)


class ChoiceDisplayField(serializers.ChoiceField):
    def __init__(self, *args, **kwargs):
        super(ChoiceDisplayField, self).__init__(*args, **kwargs)
        self.choice_strings_to_text = {
            six.text_type(key): value for key, value in self.choices.items()
        }

    def to_representation(self, instance):
        if instance is None:
            return instance
        return {
            'value': self.choice_strings_to_values.get(six.text_type(instance), instance),
            'text': self.choice_strings_to_text.get(six.text_type(instance), instance),
        }

    def to_internal_value(self, data):
        if (type(data) is str):
            data = {"value": data}

        serialized_data = data.get('value', None)
        return serialized_data


class NumpyArrayField(ArrayField):
    def __init__(self, base_field, size=None, **kwargs):
        super(NumpyArrayField, self).__init__(base_field, size, **kwargs)

    @property
    def description(self):
        return 'Numpy array of %s' % self.base_field.description

    def get_db_prep_value(self, value, connection, prepared=False):
        return super(NumpyArrayField, self).get_db_prep_value(list(value), connection, prepared)

    def deconstruct(self):
        name, path, args, kwargs = super(NumpyArrayField, self).deconstruct()
        kwargs.update({
            'base_field': self.base_field,
            'size': self.size,
        })
        return name, path, args, kwargs

    def to_python(self, value):
        return super(NumpyArrayField, self).to_python(array(value))

    def value_to_string(self, obj):
        return super(NumpyArrayField, self).value_to_string(list(obj))