# Generated by Django 2.0.2 on 2018-11-23 08:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0062_auto_20181123_0744'),
    ]

    operations = [
        migrations.AddField(
            model_name='regionset',
            name='primary_region_id',
            field=models.PositiveIntegerField(blank=True, default=0, editable=False, help_text='ID of the primary region associated with the chosen region type. The primary region corresponds to the region that is shown first when the user selects a region set.'),
        ),
    ]
