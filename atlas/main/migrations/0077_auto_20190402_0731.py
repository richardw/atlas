# Generated by Django 2.0.2 on 2019-04-02 07:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0076_auto_20190402_0707'),
    ]

    operations = [
        migrations.AlterField(
            model_name='indicator',
            name='statistics_available',
            field=models.ManyToManyField(help_text='Statistical operations available (i.e., valid) for this indicator.', related_name='statistics_available', to='main.Statistic', verbose_name='Statistics available'),
        ),
    ]
