# Generated by Django 2.0.2 on 2018-06-19 05:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0028_auto_20180619_0504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='indicator',
            name='primary_plot_key',
            field=models.SlugField(help_text='Key identifying the plot that should be shown as a default (before the user makes any selection).', max_length=999),
        ),
    ]
