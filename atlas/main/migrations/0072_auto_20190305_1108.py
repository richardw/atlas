# Generated by Django 2.0.2 on 2019-03-05 11:08

from django.db import migrations, models
import main.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0071_auto_20190305_1040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plot',
            name='plot_data_1d',
            field=main.fields.NumpyArrayField(base_field=models.DecimalField(decimal_places=4, default=None, max_digits=8, null=True), default=[], help_text='1D-Array representation of the plot data to be displayed by plotly.js', size=None),
        ),
        migrations.AlterField(
            model_name='plot',
            name='plot_data_2d',
            field=main.fields.NumpyArrayField(base_field=main.fields.NumpyArrayField(base_field=models.DecimalField(decimal_places=4, default=None, max_digits=8, null=True), size=None), default=[], help_text='2D-Array representation of the plot data to be displayed by plotly.js', size=None),
        ),
        migrations.AlterField(
            model_name='plot',
            name='plot_data_lats',
            field=main.fields.NumpyArrayField(base_field=models.DecimalField(decimal_places=4, default=None, max_digits=8, null=True), default=[], help_text='1D-Array representation of the latitude axis to be displayed by plotly.js', size=None),
        ),
        migrations.AlterField(
            model_name='plot',
            name='plot_data_lons',
            field=main.fields.NumpyArrayField(base_field=models.DecimalField(decimal_places=4, default=None, max_digits=8, null=True), default=[], help_text='1D-Array representation of the longitude axis to be displayed by plotly.js', size=None),
        ),
    ]
