# Generated by Django 2.0.2 on 2018-03-22 11:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_remove_plot_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plot',
            name='key',
            field=models.SlugField(editable=False, help_text='Unique (auto-generated) key identifying this plot.', unique=True),
        ),
    ]
