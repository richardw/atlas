# Generated by Django 2.0.2 on 2018-03-29 12:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0021_indicator_key'),
    ]

    operations = [
        migrations.AddField(
            model_name='indicator',
            name='number_of_plots',
            field=models.PositiveIntegerField(default=0, editable=False),
            preserve_default=False,
        ),
    ]
