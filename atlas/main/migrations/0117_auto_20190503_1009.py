# Generated by Django 2.2 on 2019-05-03 10:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0116_auto_20190503_0955'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='indicator',
            options={'ordering': ('order_id', 'created_at'), 'verbose_name_plural': 'indicators'},
        ),
    ]
