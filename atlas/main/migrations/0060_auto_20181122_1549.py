# Generated by Django 2.0.2 on 2018-11-22 15:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0059_indicator_netcdf_name_key'),
    ]

    operations = [
        migrations.CreateModel(
            name='RegionSet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.SlugField(editable=False, help_text='Unique (auto-generated) slug identifying this region set.', max_length=999, unique=True)),
                ('primary_region_key', models.SlugField(blank=True, help_text='Key of the primary region associated with the chosen region type. The primary region corresponds to the region that is shown first when the user selects a region set.', max_length=999)),
                ('region_type', models.CharField(help_text='Type (origin of definition) of region.', max_length=999, unique=True, verbose_name='Type')),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'region sets',
            },
        ),
        migrations.RemoveField(
            model_name='region',
            name='region_type',
        ),
        migrations.AddField(
            model_name='region',
            name='region_set',
            field=models.ForeignKey(help_text='Region set that this region is associated with.', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='regions', to='main.RegionSet'),
        ),
    ]
