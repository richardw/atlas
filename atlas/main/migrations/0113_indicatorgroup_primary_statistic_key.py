# Generated by Django 2.2 on 2019-05-02 13:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0112_auto_20190502_1052'),
    ]

    operations = [
        migrations.AddField(
            model_name='indicatorgroup',
            name='primary_statistic_key',
            field=models.SlugField(blank=True, help_text='Name of the default statistic to be chosen when the user navigates to one of the indicator groups.', max_length=999),
        ),
    ]
