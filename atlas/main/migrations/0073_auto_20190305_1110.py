# Generated by Django 2.0.2 on 2019-03-05 11:10

from django.db import migrations, models
import main.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0072_auto_20190305_1108'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plot',
            name='plot_data_1d',
            field=main.fields.NumpyArrayField(base_field=models.FloatField(default=None, null=True), default=[], help_text='1D-Array representation of the plot data to be displayed by plotly.js', size=None),
        ),
        migrations.AlterField(
            model_name='plot',
            name='plot_data_2d',
            field=main.fields.NumpyArrayField(base_field=main.fields.NumpyArrayField(base_field=models.FloatField(default=None, null=True), size=None), default=[], help_text='2D-Array representation of the plot data to be displayed by plotly.js', size=None),
        ),
        migrations.AlterField(
            model_name='plot',
            name='plot_data_lats',
            field=main.fields.NumpyArrayField(base_field=models.FloatField(default=None, null=True), default=[], help_text='1D-Array representation of the latitude axis to be displayed by plotly.js', size=None),
        ),
        migrations.AlterField(
            model_name='plot',
            name='plot_data_lons',
            field=main.fields.NumpyArrayField(base_field=models.FloatField(default=None, null=True), default=[], help_text='1D-Array representation of the longitude axis to be displayed by plotly.js', size=None),
        ),
    ]
