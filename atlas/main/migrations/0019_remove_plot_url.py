# Generated by Django 2.0.2 on 2018-03-19 13:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0018_plot_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plot',
            name='url',
        ),
    ]
