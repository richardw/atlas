# Generated by Django 2.0.2 on 2019-04-02 08:24

from django.db import migrations, models
import main.choices


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0079_auto_20190402_0820'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plot',
            name='plot_type',
            field=models.CharField(choices=[('map', 'Maps'), ('reg', 'Regional Timeseries'), ('hovlat', 'Hovmoller (latitude)'), ('hovlon', 'Hovmoller (longitude)')], default=main.choices.PlotTypeChoice('Regional Timeseries'), help_text='Type of plot.', max_length=50, verbose_name='Plot type'),
        ),
    ]
