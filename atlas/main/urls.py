from django.urls import include, path
from rest_framework import routers
from main import views


router = routers.DefaultRouter()
router.register(r'indicatorgroups', views.IndicatorGroupViewSet)
router.register(r'indicatoraggregates', views.IndicatorAggregateViewSet)
router.register(r'indicators', views.IndicatorViewSet)
router.register(r'regions', views.RegionViewSet)
router.register(r'region_sets', views.RegionSetViewSet)
router.register(r'plots', views.PlotViewSet)

urlpatterns = [
    path('', include(router.urls)),
]