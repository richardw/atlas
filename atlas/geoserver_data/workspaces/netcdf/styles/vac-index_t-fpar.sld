<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
  <NamedLayer>
    <Name>VAC-Index (T-FPAR)</Name>
    <UserStyle>
      <Name>vac-index</Name>
      <Title>VAC-Index (T-FPAR)</Title>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1.0</Opacity>
            <ColorMap type="values">
             <ColorMapEntry color="#FFFFFF" quantity="0" label="-" />
             <ColorMapEntry color="#A4CFE2" quantity="1" label="a (70%)" />
             <ColorMapEntry color="#1F77B6" quantity="2" label="a (95%)" />
             <ColorMapEntry color="#FFBD6F" quantity="3" label="b (70%)" />
             <ColorMapEntry color="#FC8100" quantity="4" label="b (95%)" />
             <ColorMapEntry color="#FC9A99" quantity="5" label="c (70%)" />
             <ColorMapEntry color="#E41A18" quantity="6" label="c (95%)" />
             <ColorMapEntry color="#B3DF8A" quantity="7" label="d (70%)" />
             <ColorMapEntry color="#33A029" quantity="8" label="d (95%)" />
           </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>