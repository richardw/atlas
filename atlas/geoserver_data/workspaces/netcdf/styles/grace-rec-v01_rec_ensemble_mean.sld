<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>GRACE TWS (mm)</Name>
    <UserStyle>
      <Name>grace-tws</Name>
      <Title>GRACE TWS (mm)</Title>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1</Opacity>
            <ColorMap>
              <ColorMapEntry color="#DA70D6" quantity="-50" label=" -50"/>
              <ColorMapEntry color="#B86A23" quantity="-20" label=" -20"/>
              <ColorMapEntry color="#E2A764" quantity="-10" label=" -10"/>
              <ColorMapEntry color="#F5E19F" quantity="-5" label=" -5"/>
              <ColorMapEntry color="#FFFFFF" quantity="0" label=" 0"/>              
              <ColorMapEntry color="#52BEA0" quantity="5" label=" 5"/>
              <ColorMapEntry color="#6EACC9" quantity="10" label=" 10"/>
              <ColorMapEntry color="#003557" quantity="20" label=" 20"/>
              <ColorMapEntry color="#000000" quantity="50" label=" 50"/>
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>