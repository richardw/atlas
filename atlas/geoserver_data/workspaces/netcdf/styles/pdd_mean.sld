<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
  <NamedLayer>
    <Name></Name>
    <UserStyle>
      <Title>Dry-to-dry transition probabilities</Title>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1</Opacity>
            <ColorMap>
              <ColorMapEntry color="#640097" quantity="0.0" label=" 0%"/>              
              <ColorMapEntry color="#222CD8" quantity="0.1" label=" 10%"/>
              <ColorMapEntry color="#598EFC" quantity="0.2" label=" 20%"/>
              <ColorMapEntry color="#8CF0DA" quantity="0.3" label=" 30%"/>
              <ColorMapEntry color="#60D092" quantity="0.4" label=" 40%"/>
              <ColorMapEntry color="#13994F" quantity="0.5" label=" 50%"/>
              <ColorMapEntry color="#92D060" quantity="0.6" label=" 60%"/>
              <ColorMapEntry color="#DAF08C" quantity="0.7" label=" 70%"/>
              <ColorMapEntry color="#FC8E59" quantity="0.8" label=" 80%"/>
              <ColorMapEntry color="#D82C22" quantity="0.9" label=" 90%"/>
              <ColorMapEntry color="#970064" quantity="1.0" label=" 100%"/>
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>