<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
  <NamedLayer>
    <Name></Name>
    <UserStyle>
      <Title>Changes in TXx</Title>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1</Opacity>
            <ColorMap>
              <ColorMapEntry color="#640097" quantity="0.0" label=" 0.0 °C"/>              
              <ColorMapEntry color="#222CD8" quantity="0.5" label=" 0.5 °C"/>
              <ColorMapEntry color="#598EFC" quantity="1.0" label=" 1.0 °C"/>
              <ColorMapEntry color="#8CF0DA" quantity="1.5" label=" 1.5 °C"/>
              <ColorMapEntry color="#60D092" quantity="2.0" label=" 2.0 °C"/>
              <ColorMapEntry color="#13994F" quantity="3.0" label=" 3.0 °C"/>
              <ColorMapEntry color="#92D060" quantity="5.0" label=" 5.0 °C"/>
              <ColorMapEntry color="#DAF08C" quantity="7.0" label=" 7.0 °C"/>
              <ColorMapEntry color="#FC8E59" quantity="10.0" label=" 10.0 °C"/>
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>