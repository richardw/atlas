<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
  <NamedLayer>
    <Name></Name>
    <UserStyle>
      <Title>Changes in CDD</Title>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1</Opacity>
            <ColorMap>
              <ColorMapEntry color="#598EFC" quantity="0" label=" 0.0 days"/>
              <ColorMapEntry color="#8CF0DA" quantity="5" label=" 5 days"/>
              <ColorMapEntry color="#60D092" quantity="10" label=" 10 days"/>
              <ColorMapEntry color="#13994F" quantity="20" label=" 20 days"/>
              <ColorMapEntry color="#92D060" quantity="30" label=" 30 days"/>
              <ColorMapEntry color="#DAF08C" quantity="40" label=" 40 days"/>
              <ColorMapEntry color="#FC8E59" quantity="50" label=" 50 days"/>
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>