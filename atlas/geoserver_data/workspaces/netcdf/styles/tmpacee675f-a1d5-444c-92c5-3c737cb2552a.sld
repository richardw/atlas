<?xml version="1.0" encoding="UTF-8"?><sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" version="1.0.0">
  <sld:NamedLayer>
    <sld:Name>Default Styler</sld:Name>
    <sld:UserStyle>
      <sld:Name>Default Styler</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:Title>raster</sld:Title>
          <sld:TextSymbolizer>
            <sld:Label>
              <ogc:PropertyName>X</ogc:PropertyName>
              <ogc:PropertyName>Y</ogc:PropertyName>
              <ogc:PropertyName>Z</ogc:PropertyName>
              <ogc:PropertyName>A</ogc:PropertyName>
              <ogc:PropertyName>B</ogc:PropertyName>
              <ogc:PropertyName>C</ogc:PropertyName>
              <ogc:PropertyName>D</ogc:PropertyName>
              <ogc:PropertyName>E</ogc:PropertyName>
            </sld:Label>
            <sld:Fill/>
          </sld:TextSymbolizer>
          <sld:RasterSymbolizer>
            <sld:ColorMap type="intervals">
              <sld:ColorMapEntry color="#A4CFE2" opacity="1.0" quantity="0"/>
              <sld:ColorMapEntry color="#1F77B6" opacity="1.0" quantity="1"/>
              <sld:ColorMapEntry color="#FFBD6F" opacity="1.0" quantity="2"/>
              <sld:ColorMapEntry color="#FC8100" opacity="1.0" quantity="3"/>
              <sld:ColorMapEntry color="#FC9A99" opacity="1.0" quantity="4"/>
              <sld:ColorMapEntry color="#E41A18" opacity="1.0" quantity="5"/>
              <sld:ColorMapEntry color="#B3DF8A" opacity="1.0" quantity="6"/>
              <sld:ColorMapEntry color="#33A029" opacity="1.0" quantity="7"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
        <sld:VendorOption name="ruleEvaluation">first</sld:VendorOption>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>

