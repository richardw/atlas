<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
  <NamedLayer>
    <Name></Name>
    <UserStyle>
      <Title>Changes in Rx5day</Title>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1</Opacity>
            <ColorMap>
              <ColorMapEntry color="#640097" quantity="-1.0" label=" -1.0 mm"/>              
              <ColorMapEntry color="#222CD8" quantity="-0.5" label=" -0.5 mm"/>
              <ColorMapEntry color="#598EFC" quantity="0" label=" 0.0 mm"/>
              <ColorMapEntry color="#8CF0DA" quantity="0.5" label=" 0.5 mm"/>
              <ColorMapEntry color="#60D092" quantity="1" label=" 1.0 mm"/>
              <ColorMapEntry color="#60D092" quantity="2" label=" 2.0 mm"/>              
              <ColorMapEntry color="#13994F" quantity="5" label=" 5.0 mm"/>
              <ColorMapEntry color="#92D060" quantity="7.5" label=" 7.5 mm"/>
              <ColorMapEntry color="#DAF08C" quantity="10" label=" 10.0 mm"/>
              <ColorMapEntry color="#FC8E59" quantity="15.0" label=" 15.0 mm"/>
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>