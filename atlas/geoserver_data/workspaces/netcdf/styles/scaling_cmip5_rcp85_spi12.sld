<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
  <NamedLayer>
    <Name></Name>
    <UserStyle>
      <Title>Changes in SPI12</Title>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1</Opacity>
            <ColorMap>
              <ColorMapEntry color="#60D092" quantity="-2.0" label=" -2.0"/>
              <ColorMapEntry color="#60D092" quantity="-1.0" label=" -1.0"/>
              <ColorMapEntry color="#222CD8" quantity="-0.5" label=" -0.5"/>
              <ColorMapEntry color="#598EFC" quantity="0" label=" 0.0"/>
              <ColorMapEntry color="#8CF0DA" quantity="0.5" label=" 0.5"/>
              <ColorMapEntry color="#60D092" quantity="1.0" label=" 1.0"/>
              <ColorMapEntry color="#60D092" quantity="2.0" label=" 2.0"/>
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>